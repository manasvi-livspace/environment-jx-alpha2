CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "launchpad-postgres",
        "USERNAME": "postgres",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "visitor_management_alpha",
        "PORT": "5432",
    },
    "EVENT": {
        "environment": "local"
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "AXLE": {
        "HOST": "http://axle.alpha.livspace.com/",
        "CLIENT_ID": "Platform-Stage",
        "CLIENT_SECRET": "q12qKlQeVkPZZnuXnhXkZe0rMjn5ERXD"
    },
    "ENV": "local",
    "APM": {
        "INDEX_NAME": "vormir-local"
    },
    "SERVICE_URL": "/vormir"
}


CLIENT_CONFIGS = {
    'launchpad_backend': {
        'axle': {
        },
        'direct': {
            'url': ' http://launchpad-backend.alpha.livspace.com',
            'headers': {

            }
        },
        'use': 'axle'
    },
    'community': {
        'axle': {
        },
        'direct': {
            'url': 'http://axle.alpha.livspace.com/civitas/api',
            'headers': {
                "X-Client-Id": "StarMSFe-G4DsKI",
                "X-Requested-By": '0',
                "X-Client-Secret": "2SXhLxG7prabkWl2rHQptbpIyt8XH9yg",
                "Content-Type": "application/json"
            }
        },
        'use': 'axle'
    },
    'alvis': {
        'axle': {
        },
        'direct': {
            'url': 'http://api.alpha.livspace.com/alvis',
            'headers': {
                "Authorization" : "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
                "X-Requested-By" : '0',
                "Content-Type" : "application/json"
            }
        },
        'use': 'axle'
    },
    'lead_service': {
        'axle': {
            'prefix': 'leadservice.alpha.livspace.com',
        },
        'direct': {
            'url': 'http://leadservice.alpha.livspace.com',
            'headers': {
            }
        },
        'use': 'axle'
    },
    'wakanda': {
        'axle': {
            'prefix': 'wakanda.alpha.livspace.com',
        },
        'direct': {
            'url': 'http://wakanda.alpha.livspace.com',
            'headers': {
            }
        },
        'use': 'axle'
    },
    'livhome-socket':{
        'direct': {
            'url': "ws://api.alpha.livspace.com:8080/socket-service/socketcluster/"
        }
    }
}