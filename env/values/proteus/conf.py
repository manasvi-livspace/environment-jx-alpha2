CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "URI_SCHEME": "mongodb+srv",
        "HOST": "canvas-mongo-test.akmgb.mongodb.net",
        "USERNAME": "livspace",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "proteus-alpha2",
    },
    "EVENT": {
        "ENV": "local"
    },
    "EVENT_SERVICE": {
        "environment": "local",
        "system_name": "proteus"
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "ENV": "local",
    # "SENTRY": {
    #     "ENABLED": True,
    #     "ENVIRONMENT": "local",
    #     "DSN": "https://8ebfd89edec64207a531c0d0b4d9ac84@sentry.livspace.com/73"
    # }
}


GATEWAY = {
    "HOST": "axle",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "LAUNCHPAD": {
        "HOST": "launchpad-backend.alpha2.livspace.com",
        "HEADERS": {}
    },
    'BOUNCER': {
        'HOST': 'api.alpha2.livspace.com/bouncer',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": False,
            "PATH": '/bouncer'
        }
    },
    "BOQ": {
        "HOST": "backoffice.alpha2.livspace.com",
        # "HOST": "localhost:1701",
        "HEADERS": {
            "Content-Type": "application/json",
            "X-Auth-Token": "8cQ904440221g0kWq8wE7N68T48DcfMr"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/backoffice'
        }
    }
}